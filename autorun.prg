* FIRO REFACCIONES
* 27 de Octubre del 2015
* Redond ear el total del documento a 5 pesos hacia abajo
* Por Manuel gtz y ammi Silva y Jose Ayala
Procedure Autorun 

	*Configurar los procedimientos de totales y prevalidacion
	*Crearlos busquedas, y su clasificaci�n
	Do Archivoplg.fxp
	
	With _Screen
		
		.AddProperty('VentasNumCliValid','Do aplicaPromoFact in autorun')
		.VentasInit 	= 'Do VentasInit	 	In AutoRun With ThisForm'
		.VentasInitDoc	= 'Do VentasInitDoc		In AutoRun With ThisForm'	
        .VentasSaveDoc  = 'Do VentasSaveDoc     In AutoRun With ThisForm'
        .VentasCancelar = 'Do VentasCancelar    In AutoRun With ThisForm'  
        .VentasUnLoad	= 'Do VentasUnLoad	    In Autorun With ThisForm'
		.AddProperty('VentasPreValida','Do Prevalida with thisForm in AutoRun')  
		.AddProperty('ArticulosGrabar','Do ArticulosGrabar With ThisForm In AutoRun')      		        
 
        *Sait CI Contabilizaci�n de Gastos y Compras. 
		.ComprasSaveDoc  = 'Do ComprasSaveDoc   In AutoRun'
		.ComprasCancelar = 'Do ComprasCancelar  In Autorun'
		
		*.AddProperty('ComprasPreValida','Do ComprasPedirTCDOF')
		.ComprasPreValida = 'Do ComprasPedirTcdof with Thisform in AutoRun'
        
        *Manejo del cami�n
		.AddProperty('gastosAgregar','Do imprimegastos in autorun')
	
	EndWith 
	
RETURN

*
*
*Funcion que corre antes de Procesar el Documento
*
*
PROCEDURE Prevalida 
LParameter oForma
Local nRec

		With oForma		
			
			If Upper(.Name) == 'FORM1' And Allt(.Caption) == 'Documentos de Venta' And .Modo == 1
			
				*Validar que la remision no tenga ningun Abono en CXC
				*Almacena el registro actual
				If Allt(.TIPODOC.value) = 'Factura'
					
					*Definiendo variables			
					cDocant2    = 'NUEVO'
					nRec = Recno('Temp')
			 
					* Recorrer el temporal
					Select Temp
					Scan
						cDocant1	    = Allt(SubStr(Temp.Docant,1,12))
					 	
					 	If 	cDocant1 <> cDocant2					 	
						 
						 	cDocant2   = Allt(SubStr(Temp.Docant,1,12))
						 	
						 	*Extraer el tipodoc de la casilla de docant 
						 	*para ver si antes era una Remision
				   			cDocant         = Temp.Docant
						 	cTipodocant 	= Allt(SubStr(cDocant,1,2))
						 	cTipodocant 	= Padl(Allt(cTipodocant),2)
						 	cNumdoc  		= Allt(SubStr(cDocant,3,12))
						 	cNumdoc  		= Padl(Allt(cNumdoc),10)
						 	
						 	If cTipodocant = ' R'	
						 		
						 		*Hay que formar tipodoc porque sino no lo reconoce
						 		cTipodoc = Allt(SubStr(cDocant,1,2))
						 		cTipodoc = Padl(Allt(cTipodoc),2)
						 		cNumdoc  = Allt(SubStr(cDocant,3,12))
						 		cNumdoc  = Padl(Allt(cNumdoc),10)
						 		
						 		cKeydoc  = cTipodoc + cNumdoc 
						 		
						 		cEnlace = ''
						 		cEnlace = Enlacerem(cNumdoc)
						 		
						 		If cEnlace = "No Pasa"
									Alerta('Debe enlazar completamente la Remision ' + '  ' + cKeydoc + '  ' + ' para poder procesar la Factura')
									_Screen.lValidaVenta = .F.
									Return .F.
								Endif
								
						 		*Buscar el modimiento de ese documento en CXC
						 		*Para generar el abono y Saldar la cuenta de la remision que acabamos
						 		*de enlazar a la factura.
						 		
								csql =  '  Select * From Cxc ' + ;  
				 	     		 '  Where ' + ;
			        	        	  '  Cxc.Keyrefer  = m.Val1        and ' + ;
			        	        	  '  Cxc.Ca        = "1"           ' + ;
			        	        	   'Order By Cxc.numcli, Cxc.Divisa, Cxc.Venc '
					 	                  
											if not sqlmsl( 'Tempabonos', csql, cKeydoc  )
												return             
											Endif
						 		
						 		Sele Tempabonos
						 		* Muestra mensaje si no se encontro soporte
								If Reccount('Tempabonos') > 0
									Alerta('La Remision' + '  ' + cKeydoc + '  ' + 'tiene abonos Registrados')
									oForma.Formapago.setfocus
									_Screen.lValidaVenta = .F.
									Return .F.
								Endif										 		
				
				 			Endif			 		
				
				 		Endif			 			
				
					EndScan
				
				Endif			
		   
		   Else
		   		
		   		_Screen.lValidaVenta = .T.
		   	
		   Endif
	  	
	  		_Screen.lValidaVenta = .T.
	  		
	  	Endwith
	
	Return

ENDPROC

*Compras SAIT CI
*Prevalida Compras
Procedure ComprasPedirTcdof
lparameter oForma
	
	WITH oForma
		
		* Fecha del documento en modo pantalla.
		*		dFechaDoc = Stod(Thisform.Fecha.Tag)
		dFechaDoc = Stod(oforma.Fecha.Tag)
		nTc = 0
		nTC = Tcdof(dFechaDoc)
		iF ntc = 0
			
			* Se Detinene
			Alerta ('Debe Registrar el Tipo de cambio DOF')
			_Screen.lValidaCompra = .F.
			Return
		
		Else 		    
			
			*Cambiamos la propiedad a ..f para que retache al usuario
	         _Screen.lValidaCompra = .T.
    	       
        Endif
    
   	    * Cambiamos esta propiedad para permitir que continue grabando el documento
	    _Screen.lValidaCompra = .T.
		
	Endwith
	 
Return 	

******************************************************
Procedure ComprasSaveDoc
Local cNumDoc	
	
	*******************************************************************
	*Procedimiento para Generar p�liza Contable
	*******************************************************************
	*Poner condici�n para Generar Poliza de Compras
	iF docum.tipodoc = ' C'
		cKeyDoc = docum.tipodoc + Docum.Numdoc	
			oConta = ContaCompras (2,cKeyDoc )
			Release oConta
	Endif

   *Devoluci�n o Nota de Cr�dito
   If docum.tipodoc = 'DC' 
		cKeyDoc = docum.tipodoc + Docum.Numdoc	
			oConta = ContaCompras (2,cKeyDoc )
   			release oConta
   endif

EndProc



*************************************************************
* ComprasCancelar
*
* Se manda llamar despues de cancelar una compra
*
Procedure ComprasCancelar
	
	*No hacer nada si no se trata de una Compra o Dev Compra
	If  (Docum.TIPODOC = ' C') or (docum.Tipodoc='DC')
		
		* Definir Variables
		cano = dtos(docum.Fecha)
		cnumper = substr(cano,1,6)
		
		Do Case
			
			Case docum.tipodoc = ' C'
				cTipoPol = "S1"
		
			Case docum.tipodoc = 'DC'
				cTipoPol = "S2"
		
		Endcase
			
		cNumpol  = Padl(allt(Docum.Numdoc),8)
		cKeyPol = cNumper + cTipoPol + cNumPol 	
		
		*Abrir tablas	
		If not opendbf('Poliza1','PERPOL') or;
			not opendbf('Poliza2','PERPOLPAR')	
			Return .F.
		Endif
		
		*Localizar p�liza para darla crank
		Sele poliza1
		=Seek(cKeyPol,'Poliza1','PERPOL')
		iF Found()
			DELETE
		Else
			Aviso ('No se encontr� p�liza')
		Endif	
		 
		Sele poliza1
		use
		
		* Localizar en Poliza2
		Sele Poliza2
		=Seek(cKeyPol + '   1','Poliza2','PERPOLPAR')
		Scan
			If (Poliza2.Numper + Poliza2.tipopol + poliza2.numpol)== cKeyPol
				delete
			endif
		Endscan
				
		Sele poliza2
		use
		
	EndIf

Return

*******************************************************************
*Procedimiento para Generar p�liza Contable de Gastos
*******************************************************************

Procedure ImprimeGastos

	If Gastos.conc = 'GA'

		cKeyDoc = Gastos.numprov + Gastos.numdoc	
		oConta = ContaGastos (4,cKeyDoc, Gastos.Idregla )
		Release oConta

	Endif

	* OBTENER EL NOMBRE DEL PROVEDOR PARA EL REPORTE.
	Sele provedor
	=Seek(gastos.numprov,'provedor','NUMPROV')
	cNomProv = Provedor.NOMPROV

	SELE GASTOS
		cnumprov = gastos.numprov
		cdoc 	 = gastos.numdoc
	    dfecha	= gastos.fecha
	    dvenc	= gastos.venc
	    mdatos	= allt(gastos.datos)
	    mobs	= allt(gastos.obs)
	    cdivisa	= gastos.divisa
	    nregla	= gastos.idregla
	    ntc1	= gastos.tc
	    ntotal1	=  gastos.total
		sendrep ('ftogasto')
Return


*****************************************************************
*
* VentasInit
*
* Se manda llamar al abrir la ventana de registro de ventas
*
Procedure VentasInit
LParameter oForma
Local nTotal,cNumCli
	
	With oForma
		
		*
		* Evaluar si estoy en la ventana de documentos de venta
		*
		If Upper(.Name) == 'FORM1' And Allt(.Caption) == 'Documentos de Venta'  And .Modo <> 1 And Left(.LlaveDoc,2) == ' F'
			* Estoy consultando una Factura
			* Abrir bases de datos
			If Not OpenDbf('Docum','TIPONUM')
				Return .F. 
			EndIf
			
			* Evaluar si esta abierta la ventana de informacion adicional del pedido
			If Type('oForma.oVtaInfPed.Caption') <> 'C'
				
				* Crea una propiedad en la ventana de registro de ventas
				.AddProperty('oVtaInfPed','')
				
				* Manda llamar la ventana de informacion adicional del pedido
				DoForm('VtaInfPed',oForma)
				
			EndIf
			 
			* Almacenar en una propiedad de la forma la llave del documento
			 .oVtaInfPed.cKeyDocum = .LlaveDoc
			
			* Manda llamar el metodo para cargar la informacion adicional del pedido
			.oVtaInfPed.CargarInfo()
			
		EndIf
		
	EndWith
	
Return


**********************************************************************************
* VentasInitDoc
*
* Se manda llamar al inicializar un documento de venta
*
Procedure VentasInitDoc
LParameter oForma
Local nRec, lEsRed, cDesc, nRecnoArts, cKeyDocum, nTotal
		
	With oForma 
	
		*
		* Evaluar si estoy en la ventana de documentos de venta
		*
		If Upper(.Name) == 'FORM1' And Allt(.Caption) == 'Documentos de Venta' And .Modo == 1
				
			* Estoy agregando un nuevo pedido
			* Evaluar si es un pedido
			If Left(.TipoDoc.Value,1) == 'F'
			
				* Evaluar si esta abierta la ventana de informacion adicional del pedido
				If Type('oForma.oVtaInfPed.Caption') <> 'C'
					
					* Crea una propiedad en la ventana de registro de ventas
					.AddProperty('oVtaInfPed','')
					
					* Manda llamar la ventana de informacion adicional del pedido
					DoForm('VtaInfPed',oForma)
					
				Else
					
					* Mostrar la ventana de informacion adicional
					.oVtaInfPed.Visible = .T.

					*Regresar el no a la ventana de pedido
					.oVtaInfPed.Envped.value = 2
					.oVtaInfPed.Envped.InteractiveChange()
					.oVtaInfPed.CargarInfo() 
					
				EndIf
				
				* Ocultar el boton para que pueda modificar la informacion adicional del pedido
				.oVtaInfPed.Modificar.Visible = .F.
				
			Else
				
				* Ocultar la ventana de informacion adicional
				If Type('oForma.oVtaInfPed.Caption') == 'C'
					.oVtaInfPed.Visible = .F.
				EndIf
				 
			EndIf
			
		EndIf 

	Endwith
				
	**************************************************************************************************************************
	************************************************************redondeo******************************************************
	**************************************************************************************************************************
	* Almacena el registro actual
	*	nRec = Recno('Temp')
	*	lEsRed = .F. 
 	*
	*	* Recorrer el temporal
	*	Select Temp
	*	Scan
	*		
	*		* Revisar si ya existe el articulo del redondeo
	*		If Allt(Temp.NUMART) == 'RED'
	*			lEsRed = .T.
	*		EndIf
	*		
	*	EndScan
	*	GoTo nRec
	*	
	*	* Si no existe el articulo del redondeo en el temporal, agregarlo
	*	If Not lEsRed
	*		
	*		* Buscar la clave del articulo del redondeo para obtener la descripcion
	*		cDesc = ''
	*		nRecnoArts = 1
	*		If Seek(PadL('RED',20),'Arts')
	*			cDesc = Arts.DESC
	*			nRecnoArts = Recno('Arts')
	*		EndIf
	*		
	*		* Agregar al temporal el redondeo
	*		Select Temp
	*		Replace NUMART		With	'RED',;
	*				DESC		With	cDesc,;
	*				CANT		With	1,;
	*				PREUNI5		With	-999999.99999,;
	*				EQUIV1		With	1,;
	*				EQUIV2		With	1,;
	*				ARTPRECIO5	With	-999999.99999,;
	*				ESSERVICIO	With	.T.,;
	*				RECNOARTS	With	nRecnoArts,;
	*				UNIDAD		WITH	'PZA'
	*	EndIf
	**************************************************************************************************************************
	**************************************************************************************************************************
	**************************************************************************************************************************
	
   Return

	*			*****************************************************************************
	*			* VentasActTot2
	*			*
	*			* Se manda llamar automaticamente al actualizar los totales del documento
	*			* cada que se cambia de cliente, articulo, cantidad y precio
	*			*
	*			Procedure VentasActTot2
	*			LParameter oForma
	*			Local nRed, nRec
	*				
	*				* Inicializar variables
	*				nRed = 0
	*				nRec = 1
	*				
	*				Select Temp
	*				Scan
	*					
	*					* Revisar si el articulo no es el redondeo
	*					If Allt(Temp.NUMART) <> 'RED'
	*						* Sumar el total de la venta
	*						nRed = nRed + (Temp.CANT*Temp.PRECIO*(1-Temp.PJEDESC/100)*(1+Temp.IMPUESTO1/100))
	*					Else
	*						* Es el articulo del redondeo
	*						* Almacenar el registro en el que se localiza
	*						nRec = Recno('Temp')
	*					EndIf
	*					
	*				EndScan
	*				
	*					If Mod(nRed,5)>0
	*						
	*						* Redondear el total del documento a 5 pesos hacia abajo
	*						nRed = -Mod(nRed,5)
	*						
	*						Select Temp
	*						GoTo nRec
	*						Replace	PRECIO		With	nRed/(1+Temp.IMPUESTO1/100)
	*						
	*					EndIf					
	*			Return


Procedure AplicaPromoFact
 	
 	  * Revisar si tiene facturas vencidas

       *AVISO DE CLIENTE SUSPENDIDO
       IF CLIENTES.SUSPENDIDO 
	      Alerta('El cliente tiene el cr�dito Suspendido')
	   endif       
  
  If Not Type('oForma.DatosCliente.Tag') == 'U'
  
	    If .DetenerCreditoConFacturasVencidas
	   
	        * 7/Ago/2005 Si el cliente tiene 0 dias de credito => NO Credito
	        * If Clientes.DIASCRED==0
	        *   Alerta('Cliente no tiene definido el n�mero de d�as que se le otorgan de cr�dito')
	        *       .NumCli.SetFocus
	        *    Return .f.
	        * Endif

	        cSql = 'Select * From Cxc Where NUMCLI==Val1 and CONC=="FA" and SALDO>0 Order By VENC'
	        If not SqlMsl('TempCxc',cSql,Clientes.NUMCLI)
	            .NumCli.SetFocus
	            Return .f.
	        Endif
	        Wait Clear
	        Select TempCxc
	        dMasAntigua = TempCxc.VENC       
	        DelAlias('TempCxc')

	        * Determinar dias de gracia a usar el global o por cliente
	        nDGraciaCli = Val(ValProp(Clientes.OTROSDATOS,'DGracia'))
	        If nDGraciaCli>0
	            nDiasDeGracia= nDGraciaCli
	        Else
	            nDiasDeGracia = .DiasDeGraciaEnFacturasVencidas
	        Endif

	        If not Empty(dMasAntigua) and dMasAntigua + nDiasDeGracia < Date()
	            If Empty(Cia.AUTO01)
	                Alerta('Cliente tiene facturas vencidas')
	        *        .NumCli.SetFocus
	                Return .f.
	            Else
	                If not Autoriza('01','Cliente tiene facturas vencidas')
	         *           .NumCli.SetFocus
	                    Return .f.
	                Endif
	            Endif
	        Endif
	       
	    Endif
	
	Endif
	
RETURN
 
 
*
* VentasSaveDoc
*
* Se manda llamar automaticamente, despues de que se agrega un documento de venta
*
Procedure VentasSaveDoc
LParameter oForma

	*Procedure VentasSaveDoc
	*******************************************************************
	*Procedimiento para Generar p�liza Contable
	*******************************************************************
	*Si es factura 
	iF Docum.tipodoc = ' F'
		cKeyDoc = docum.tipodoc + Docum.Numdoc	
		oConta = ContaVentas (1,cKeyDoc )
		Release oConta
	Endif

   *Devoluci�n o Nota de Cr�dito
   If Docum.tipodoc = 'DV' 
		cKeyDoc = docum.tipodoc + Docum.Numdoc	
		oConta = ContaVentas (1,cKeyDoc )
   		Release oConta
   Endif
   
   
   *Facturar Nota. Grabo venededor de la nota.
   If Docum.tipodoc = ' F' and Docum.Fueticket = .T.
		
		cKeyDoc = docum.tipodoc + Docum.Numdoc
		cObsTotal    = Allt(Docum.Obs)
		cObsNotas    = SubStr(cObsTotal, 18,200)
		cNumVend = ''
		cNumVend = VendedordelaNota (1,cKeyDoc, cObsNotas)
		
	*Cambiar el n�mero de vendedor.
	Sele Docum
		Replace docum.numvend with cNumVend
	ENDIF
    
   
   *******************************************************************************************************
   *Remisiones
   *******************************************************************************************************
   *
   * VentasSaveDoc
   *
   * Se manda llamar automaticamente, despues de que se agrega un documento de venta
   * 
	
	Local cKeyDocum,cCampos
	
	* Evaluar si proceso un Factura
	If Docum.TIPODOC == ' F'

		cDocant2    = 'NUEVO'
		cTiponum    = Docum.tipodoc + Docum.Numdoc
		cTiponumpar = Docum.tipodoc + Docum.Numdoc + '  1'
		nTotal      = (Docum.Importe+Docum.Impuesto1+Docum.Impuesto2)-Docum.Descuento
		cReferF     = "FA" + Allt(Docum.Numdoc)
		
		*Buscar en Movim la partida que acabamos de grabar
		Sele Movim
		 =Seek(cTiponumpar,'Movim','Tiponumpar')
		 If found()
		   Scan while cTipoNum = Movim.Tipodoc + Movim.numdoc
		   
		   cDocant1	    = Allt(SubStr(Movim.Docant,1,12))

			 	If cDocant1 <> cDocant2
				 	
				 	nImporte   = 0
				 	cDocant2   = Allt(SubStr(Movim.Docant,1,12))
				 	
				 	*Extraer el tipodoc de la casilla de docant 
				 	*para ver si antes era una Remision
		   			cDocant         = Movim.Docant
				 	cTipodocant2 	= Allt(SubStr(cDocant,1,2))
				 	cTipodocant 	= Padl(Allt(cTipodocant2),2)
				 	
				 	
				 	If cTipodocant = ' R'
				 		
				 		*Hay que formar tipodoc porque sino no lo reconoce
				 		cTipodoc2 = Allt(SubStr(cDocant,1,2))
				 		cTipodoc = Padl(Allt(cTipodoc2),2)
				 		cNumdoc2  = Allt(SubStr(cDocant,3,10))
				 		cNumdoc  = Padl(Allt(cNumdoc2),10)
				 		
				 		cKeydoc  = cTipodoc + cNumdoc
				 						 		
						csql =  '  Select * From Cxc ' + ;  
		 	     		 '  Where ' + ;
	        	        	  '  Cxc.Keyrefer  = m.Val1        and ' + ;
	        	        	  '  Cxc.Ca        = "1"           ' + ;
	        	        	   'Order By Cxc.numcli, Cxc.Divisa, Cxc.Venc '
			 	                  
						if not sqlmsl( 'Tempabonos', csql, cKeydoc  )
							return             
						Endif
						
						If Reccount('Tempabonos') > 0
						
					 		*Buscar el movimiento de ese documento en CXC
					 		*Para generar el abono y Saldar la cuenta de la remision que acabamos
					 		*de enlazar a la factura por el importe que le toque.
					 		Sele Cxc
					 		=Seek(cKeydoc,'CXC','Keycxc')
					 		If found()
					 			
					 			Sele Cxc
					 			cKeyDocum 	= Cxc.keydocum
					 			cNumdoc   	= Cxc.Numdoc	
					 			cObs      	= Cxc.Obs
								cRefer    	= Padr(Allt(Cxc.Refer),12)
								cFecha    	= Cxc.Fecha
								nImporteCxc	= Cxc.Saldo 
								nImporte  	= nTotal
								cObs      	= Cxc.obs
								cDivisa	  	= Cxc.Divisa
								nAbono      = Cxc.Importe - Cxc.Saldo
								
								iF nImporteCxc > 0
								
						 			Sele Cxc
						 				Replace Saldo with iiF(nImporteCxc - nImporte <= -1,0,nImporteCxc - nImporte)
									
									*En el caso que sea negativo y solo hay que generar el abono
									*Por el total de la Cxc
			 						if nImporteCxc - nImporte <= -1
									   
									   nImporte =  nImporteCxc
									  
									Endif
									
								Else
									
									*Me salgo de este docuemnto ya que esta totalmente 
									*pagado y ya tiene abonos registrados
									LOOP
									
								Endif
								
							Else
							
								Aviso('No se encontro la Cxc')
							
							Endif
						
						Else
							
							Sele Cxc
					 		=Seek(cKeydoc,'CXC','Keycxc')
					 		If found()
					 			nImporte  	= Cxc.Saldo
					 		Endif
					 			
							cKeyDocum 	= cTipodoc + cNumdoc
				 			cNumdoc   	= cNumdoc	
				 			cObs      	= ""
							cRefer    	= "RM" + Allt(cNumdoc)
							cFecha    	= Date()
							nImporteCxc	= nTotal 
							cDivisa	  	= Docum.Divisa
								
						Endif
						
						*Revisar  que tenga saldo el documento
						iF  nImporte > 0
							
							*Quitarle el saldo al cliente para que vuelva a tener saldo disponible
						    Opendbf('Clientes','Numcli')
							Sele Clientes
							=Seek(Padl(allt(Docum.Numcli),5),'Clientes','Numcli')
							If found()
								
								nSaldo = Clientes.Saldo
								
								Sele Clientes
							   		Replace Saldo With nSaldo - nImporte
				

							Endif
				 		
					 		*Scar el Consecutivo de la Cxc
					 		Sele Cia
							Replace Cia.sigcxc with Sigdoc(Cia.sigcxc)
							
							*Sacar fecha y Hora
							cFechaCx = DTOC(DATE())
							cDia     = Sublin(Allt(cFechaCx),1,'/')
							cMes     = Sublin(Allt(cFechaCx),2,'/')
							cAno     = Sublin(Allt(cFechaCx),3,'/')
							
							cFechaH  = DATETIME() 
							cHoraH   = TTOC(cFechaH, 2)
							cHora    = Sublin(Allt(cHoraH),1,':')
							cMin     = Sublin(Allt(cHoraH),2,':')
							cSeg     = Sublin(Allt(cHoraH),3,':')
							
							cNumUser = Numuser
							
							*Agrego El abono para pagar la Remision
							*Crear Concepto de "RF" = Remision Facturada
							Sele Cxc
								Append Blank
									Replace Numcli	    with 	Docum.Numcli,;
											Conc 	    with 	'RF',;
											Numdoc	    with 	Allt(Docum.Numdoc),; 
											Refer	    with	cRefer,;
											Fecha	    with	cFecha,;
											Importe	    with	nImporte,;
											Obs		    with	cObs,;
											Tc			with	0,;
											Divisa		with	cDivisa,;
											Saldo		with	0,;
											CA			with	'1',;
											Recno		with	Recno(),;
											Numuser		with	cNumUser,;
											Numalm		with	_Numalm,;
											keycxc		with	Padl(Allt(Cia.sigcxc),12),;
											keyrefer	with	cKeydoc,;
											keyrefer2	with	'',;
											keydocum	with	cKeyDocum,;
											keycaja		with	'',;
											FechaHora	with	cAno+cMes+cDia+cHora+cMin+cSeg
								    
						    Sele Cia
								Replace Cia.sigcxc with Padl(Allt(Sigdoc(Cia.Sigcxc)),12)
							
							*Actuaizar Sado de Cxc de la Remision
							Sele Cxc
							Go Top
							=Seek(cTipodoc+cNumdoc,'Cxc','Keycxc')
							iF Found()
								
								Sele Cxc
								nSaldoCxc = Cxc.Saldo
								
								Sele Cxc
									Replace	Saldo	with	nSaldoCxc - nImporte
									
							Endif
								
							*Agrego abo no a factura para pagar 
							*Si la remision ya tenia pagos registrados para 
							*que la factura quede Igual
							If Reccount('Tempabonos') > 0
							
								Sele Cxc
									Append Blank
										Replace Numcli	    with 	Docum.Numcli,;
												Conc 	    with 	'FR',;
												Numdoc	    with 	Allt(Docum.Numdoc),; 
												Refer	    with	cReferF,;
												Fecha	    with	cFecha,;
												Importe	    with	nAbono,;
												Obs		    with	cObs,;
												Tc			with	0,;
												Divisa		with	cDivisa,;
												Saldo		with	0,;
												CA			with	'1',;
												Recno		with	Recno(),;
												Numuser		with	cNumUser,;
												Numalm		with	_Numalm,;
												keycxc		with	Padl(Allt(Cia.sigcxc),12),;
												keyrefer	with	Docum.Tipodoc + Docum.Numdoc,;
												keyrefer2	with	'',;
												keydocum	with	Docum.Tipodoc + Docum.Numdoc,;
												keycaja		with	'',;
												FechaHora	with	cAno+cMes+cDia+cHora+cMin+cSeg
									   
									   *Importe	    with	nTotal - nImporte,;
									   
							    Sele Cia
								Replace Cia.sigcxc with Padl(Allt(Sigdoc(Cia.Sigcxc)),12)
									
								*Actuaizar Sado de Cxc de la factura 
								*y de Ciente
								Sele Cxc
								Go Top
								=Seek(Docum.Tipodoc+Docum.Numdoc,'Cxc','Keycxc')
								iF Found()
									
									Sele Cxc
									nSaldoCxc = Cxc.Saldo
									
									Sele Cxc
									     Replace	Saldo	with	nSaldoCxc - nAbono
								
								Endif
								
							    Opendbf('Clientes','Numcli')
								Sele Clientes
								Go Top
								=Seek(Padl(allt(Docum.Numcli),5),'Clientes','Numcli')
								If found()
									
									nSaldoA = Clientes.Saldo
									
									Sele Clientes
								   		Replace Saldo With nSaldoA - nAbono
					

								Endif
							
							Endif
							
						Endif
						
				 	Endif	
			 	
			 	Endif
			 
			 Endscan	
		 
	     Endif
		
	EndIf
	
	**********************************************************************************TERMINA REMISIONES SAVE DOC   
	*Filtrar Tipo de Documento, ya se grabo el documento y ya estamos en el renglon donde se agrego.
	IF Docum.TIPODOC = ' F' And Docum.Factdiaria <> .T.
		
		IF Docum.TIPODOC = ' F' and Docum.Fueticket <> .T.
			
			***********************************************
			*Buscar si ya existe una ventana de ventas abierta
			*AQUI METI EL PROCEDIMIENTO PARA AGREGAR RUTAS
			WITH oForma.oVtaInfPed
		 		
		 		If .Envped.value = 1
		 		
			 		local cSigRuta
			 		cSigRuta = ''
			 		
			 		* Abrir tabla de rutas para guardar datos		
			 		If  Not Opendbf('Rutas','numRuta') OR;
			 			Not Opendbf('Ciaws')
						Return .f.
					Endif
			 	
			 		Sele Ciaws
			 			cSigRuta = Padl(Allt(Ciaws.SigRuta),10)
			 			
					  		
			  		Sele Rutas
			  		If .Metodo.value = 1
				    	
				    	Sele Rutas	
				    		Appen Blank
					    		Replace Numruta 	with padl(allt(cSigRuta),10),;
					    				Tipodoc 	with Docum.Tipodoc,;
					    				Numdoc  	with Docum.numdoc,;
					    				Numcli  	with Docum.numcli,;
					    				Fecha   	with Docum.fecha,;
					    				Numclisuc 	with Docum.numclisuc,;
					    				Numcam		with .Numcam.value,;
					    				Metodo      with .Metodo.value,;
					    				Nomuser		with m.NomUser,;
					    				Cerrar		with .F.,;
					  	 				Almacen		with .F.,;
					  	 				Logistica	with .F.,;
					  	 				Recibido	with .F.
					  	 				
				    
				    Else
				    	
				    	Sele Rutas
				    		Appen Blank
					    		Replace Numruta 	with padl(allt(cSigRuta),10),;
					    				Tipodoc 	with Docum.Tipodoc,;
					    				Numdoc  	with Docum.numdoc,;
					    				Numcli  	with Docum.numcli,;
					    				Fecha   	with Docum.Fecha,;
					    				Numclisuc 	with Docum.Numclisuc,; 
					    				Numguia     with .Numguia.value,;
					    				Numpaq      with .Numpaq.value,;
					    				Metodo      with .Metodo.value,;
					    				Nomuser		with m.NomUser,;
					    				Cerrar		with .F.,;
					    				Almacen		with .F.,;
					    				Logistica	with .F.,;
					  	 				Recibido	with .F.
					     											    
				    Endif
					
					*Actualizar Folio de Rutas
					Sele Ciaws
			    		cNewRuta = SigDoc(cSigRuta)
			    		Replace	SigRuta	with	Padl(Allt(cNewRuta),10)
			    	USE	
			    	 	 		 		    								    						    							    
				    *Cerrar tablas
			    	Sele Rutas 
			    	Use
			    	
			    	*Guardar la ruta en docum
			    	Sele Docum
			    		Replace Numruta with padl(allt(cSigRuta),10),;
			    				Numguia with .numguia.value					    		    		    	
				 	Endif
			 
			Endwith 
				 
		ENDIF
		
	Endif
	
	*Si es una nota que se va a facturar
	IF Docum.Tipodoc = ' F' and Docum.Fueticket == .T.
	
		Sele Docum
		cNumdocKey = Padl(Allt(Docum.Numdoc),10)
		
 		*Manda llamar la ventana de informacion adicional del pedido
		DoForm('VtaInfNota',cNumdocKey)

 	Endif
	
	* Evaluar si proceso un Factura
	If Docum.TIPODOC == ' F'
		
		cNumdocfac 	= Padl(Allt(Docum.Numdoc),10)
		
		cTipoNumPar = ' F' + cNumdocfac + '  1'
		
		Sele Movim
		=Seek(cTipoNumPar,'Movim','TipoNumPar')
		If found()
		
			Sele Movim 
			cObs 	   = Allt(Movim.Obs)
			cDescObs   = Sublin(Allt(cObs),1,':') 
			cMovimObs  = Sublin(Allt(cObs),2,':')
			
			iF cDescObs = 'ARTICULOS VENDIDOS EN LA NOTA' OR cDescObs = 'ARTICULOS VENDIDOS EN LAS NOTAS'
				
				*Crear cursor para guardar las notas
				Create Cursor TempgNotas(;
					Numdoc	C(10))
					
				cListaNotas = Allt(cMovimObs)
				nTotal = ItemsInList(cListaNotas)
				
				*Cambiar Status de las notas
				For i=1 to nTotal

					cNumDoc = ItemOfList(i,cListaNotas)
					cNumDoc = Padl(Allt(cNumdoc),10)
				
					cNumdocN = ' N'+ cNumDoc  
					
					*Cambiar el Status en la Nota
					Sele Docum 
					=Seek(cNumdocN,'Docum','Tiponum') 
					If Found()
						Sele Docum
							Replace	Status		with	5,;
									Numfact		with	cNumDocfac
						
						Sele TempgNotas
							Append Blank
								Replace	Numdoc with	Padl(Allt(cNumDoc),10)
						
					Else
					
						Alerta('No se encuentra la Nota para cambiar Status: ' + cNumdocN)
						Return .F.
					
					Endif
					
					*Cambiar el Status en la Factura
					cTipoNum = ' F' + cNumDocfac
					Sele Docum
					Go Top
					=Seek(cTipoNum,'Docum','Tiponum')
					If Found()
						Sele Docum
							Replace	Obs			with	cObs,;
									FactDiaria	with	.T.,;
									Fueticket	with	.F.
					
					Endif

				Endfor
				
				*Enviar evento de las notas que se les cambio el Status
				Sele TempgNotas
				Go top
				Scan
					
					cNumDocNt = Padl(Allt(TempgNotas.Numdoc),10)
					cNumdocNota = ' N'+ cNumDocNt  
					
					*Cambiar el Status en la Nota
					Sele Docum 
					=Seek(cNumdocNota,'Docum','Tiponum') 
					If Found()
					
						*Mandar evento del cambio
						Sele Docum
						SDAddEvent ('AddNota',cNumdocNota,"","",2)
					
					Endif

				Endscan
				
				*Borrar Cursor
				Sele TempgNotas
				Zap
				
				*Selecionar Docum de la factura
				Sele Docum
				=Seek(' F' + cNumdocfac,'Docum','TipoNum')
			
			Endif
			
		Endif
		
	Endif
	
Return

 
*
* VentasCancelar
*
* Se manda llamar automaticamente, despues de Cancelar un Documento
*
Procedure VentasCancelar
LParameter oForma
	 
	************************** Cancelar o borrar p�liza
	* VentasCancelar
	*
	* Se manda llamar despues de cancelar un documento
	*
	*Procedure VentasCancelar
	*Local nArea,nReg,cDocant,cNumDoc
		
		* No hacer nada si no se trata de una factura REMISION O NOTA
		If  (Docum.TIPODOC = ' F') or (docum.Tipodoc='DV')
			* Definir Variables
				cano = dtos(docum.Fecha)
				cnumper = substr(cano,1,6)
				Do Case
					Case docum.tipodoc = ' F'
					cTipoPol = "F1"
					Case docum.tipodoc = 'DV'
					cTipoPol = "F2"
				Endcase
					
				cNumpol  = Padl(allt(Docum.Numdoc),8)
		
				cKeyPol = cNumper + cTipoPol + cNumPol	
			* Abrir tablas	
			If not opendbf('Poliza1','PERPOL') or;
				not opendbf('Poliza2','PERPOLPAR')	
				RETURN .F.
			ENDIF
			
			* Localizar p�liza para darla crank
			Sele poliza1
			=Seek(cKeyPol,'Poliza1','PERPOL')
			IF FOUND()
				DELETE
			ELSE
				Aviso ('No se encontr� p�liza')
			endif	
			Sele poliza1
			use
			
			* Localizar en Poliza2
			Sele Poliza2
			=Seek(cKeyPol + '   1','Poliza2','PERPOLPAR')
			Scan
				If (Poliza2.Numper + Poliza2.tipopol + poliza2.numpol)== cKeyPol
					delete
				endif
			endscan
					
			Sele poliza2
			use
			 
		EndIf 
	
	*Para borrar la ruta si se cancela el documento
	If  NOT OPENDBF('Rutas','numRuta')
		Return .f.
	Endif
	
	IF Docum.TIPODOC = ' F' And Docum.Status = 1

		cNumruta = Padl(Allt(Docum.Numruta),10)
		
		If Not Empty(cNumruta)
			Sele Rutas
		    =Seek(cNumruta,'Rutas','NumRuta')
		    If found()
				Delete
				Aviso('La Ruta ha sido Eliminada de la Bit�cora') 
			Endif
		Endif
	
	Endif
	
	Sele Rutas
	use
		
	* Aqui empieza el procedimiento de remisiones
	*
	* VentasCancelar
	*
	* Se manda llamar automaticamente, despues de Cancelar un Documento
	*
	* 
	*Procedimiento para quitar Remisiones
	IF Docum.TIPODOC = ' F' And Docum.Status = 1

		cDocant2    = 'NUEVO'
		cTiponum    = Docum.tipodoc + Docum.Numdoc
		cTiponumpar = Docum.tipodoc + Docum.Numdoc + '  1'
		 
		*Buscar en Movim la partida que acabamos de grabar
		Sele Movim
		 =Seek(cTiponumpar,'Movim','Tiponumpar')
		 If found()
		   Scan while cTipoNum = Movim.Tipodoc + Movim.numdoc
		   
		   cDocant1	    = Allt(SubStr(Movim.Docant,1,12))

			 	If cDocant1 <> cDocant2
				 	
				 	cDocant2   = Allt(SubStr(Movim.Docant,1,12))
				 	
				 	*Extraer el tipodoc de la casilla de docant 
				 	*para ver si antes era una Remision
		   			cDocant      = Movim.Docant
				 	cTipodocant 	= Allt(SubStr(cDocant,1,2))
				 	cTipodocant 	= Padl(Allt(cTipodocant),2)
				 	
				 	If cTipodocant = ' R'
				 		
				 		*Hay que formar tipodoc porque sino no lo reconoce
				 		cTipodoc = Allt(SubStr(cDocant,1,2))
				 		cTipodoc = Padl(Allt(cTipodoc),2)
				 		cNumdoc  = Allt(SubStr(cDocant,3,10))
				 		cNumdoc  = Padl(Allt(cNumdoc),10)
				 		
				 		cKeydoc  = cTipodoc + cNumdoc
				 		nImporte = 0
				 		
				 		*Buscar el modimiento de ese documento en CXC
				 		*Para colocar el saldo de nuevo en las remisiones luego ir a borrar los abonos
				 		*de enlazar a la factura.
				 		*Mandar a cero saldo de la remision
				 		Sele Cxc
				 		Go Top
				 		=Seek(cKeydoc,'CXC','Keycxc')
				 		If found()
							
							nImporte = Cxc.importe
				 			Sele cxc
				 				Replace Saldo with nImporte
				 			
				 			cKeyDocum = Cxc.keydocum
				 			cNumdoc   = Cxc.Numdoc	
				 			cObs      = Cxc.Obs
							cRefer    = Padr(Allt(Cxc.Refer),12)
							cFecha    = Cxc.Fecha
							cObs      = Cxc.obs		
				 		
						
						Endif
 						
						iF nImporte > 0
						
							*Agregarle el saldo al cliente para que vuelva a tener saldo de las remisiones que habia quitado
						    Opendbf('Clientes','Numcli')
							Sele Clientes
							=Seek(Padl(allt(Docum.Numcli),5),'Clientes','Numcli')
							If found()
								nSaldo = Clientes.Saldo
							    Replace Clientes.saldo With nSaldo + nImporte
							Endif
							
						Endif
		
						*Buscar el modimiento de ese documento en CXC
				 		*Para generar el abono y Saldar la cuenta de la remision que acabamos
				 		*de enlazar a la factura.
				 		
				 		csql =  '  Select * From Cxc ' + ;  
			 	     		 '  Where ' + ;
		        	        	  '  Cxc.Keyrefer  = m.Val1        and ' + ;
		        	        	  '  Cxc.Ca        = "1"           ' + ;
		        	        	   'Order By Cxc.numcli, Cxc.Divisa, Cxc.Venc '
				 	                  
										if not sqlmsl( 'Tempabonos', csql, cKeydoc  )
											return             
										Endif
						
						Sele Tempabonos
						Scan
							cKeycxc = Tempabonos.Keycxc
							Sele Cxc
					 		=Seek(cKeycxc,'CXC','Keycxc')
					 		If found()
					 			Sele Cxc
					 			Delete
					 		Endif
					 	Endscan
							
				 	Endif	
			 	
			 	Endif
			 
			 Endscan	
		 
	     Endif
		
	EndIf
	
	*Evaluar si proceso un Factura
	If Docum.TIPODOC == ' F'
		
		cNumdocfac 	= Padl(Allt(Docum.Numdoc),10)
		
		cTipoNumPar = ' F' + cNumdocfac + '  1'
		
		Sele Movim
		=Seek(cTipoNumPar,'Movim','TipoNumPar')
		If found()
		
			Sele Movim 
			cObs 	   = Allt(Movim.Obs)
			cDescObs   = Sublin(Allt(cObs),1,':') 
			cMovimObs  = Sublin(Allt(cObs),2,':')
			
			iF cDescObs = 'ARTICULOS VENDIDOS EN LA NOTA' OR cDescObs = 'ARTICULOS VENDIDOS EN LAS NOTAS'
				
				*Crear cursor para guardar las notas
				Create Cursor TempgNotas(;
					Numdoc	C(10))
					
				cListaNotas = Allt(cMovimObs)
				nTotal = ItemsInList(cListaNotas)
				
				*Cambiar Status de las notas
				For i=1 to nTotal  

					cNumDoc = ItemOfList(i,cListaNotas)
					cNumDoc = Padl(Allt(cNumdoc),10)
				
					cNumdocN = ' N'+ cNumDoc  
					
					*Cambiar el Status en la Nota
					Sele Docum 
					=Seek(cNumdocN,'Docum','Tiponum') 
					If Found()
						Sele Docum
							Replace	Status		with	0,;
									Numfact		with	''
						
						Sele TempgNotas
							Append Blank
								Replace	Numdoc with	Padl(Allt(cNumDoc),10)
						
					Else 
					
						Alerta('No se encuentra la Nota para cambiar Status: ' + cNumdocN)
						Return .F.
					
					Endif
					
					*Cambiar el Status en la Factura
					cTipoNum = ' F' + cNumDocfac
					Sele Docum
					Go Top
					=Seek(cTipoNum,'Docum','Tiponum')
	
				Endfor
				
				*Enviar evento de las notas que se les cambio el Status
				Sele TempgNotas
				Go top
				Scan
					 
					cNumDocNt = Padl(Allt(TempgNotas.Numdoc),10)
					cNumdocNota = ' N'+ cNumDocNt  
					
					*Cambiar el Status en la Nota
					Sele Docum 
					=Seek(cNumdocNota,'Docum','Tiponum') 
					If Found()
							
						*Mandar evento del cambio
						Sele Docum
						SDAddEvent ('AddNota',cNumdocNota,"","",2)
					
					Endif

				Endscan
				
				*Borrar Cursor
				Sele TempgNotas
				Zap
				
				*Selecionar Docum de la factura
				Sele Docum
				=Seek(' F' + cNumdocfac,'Docum','TipoNum')
			
			Endif
			
		Endif
		
	Endif
	
Return


**********************************************************************************
* PrevalidaVentas
*
Procedure PreValidaVentas
LParameter oForma
		
Return


*
* VentasUnLoad
*
* Se manda llamar al cerrar la ventana de registro de ventas
*
Procedure VentasUnLoad
LParameter oForma
	
	* Evaluar si esta abierta la ventana de informacion adicional del pedido
	If Type('oForma.oVtaInfPed.Caption') == 'C'
			* Cerrar la ventana de informacion adicional del cliente
		oForma.oVtaInfPed.Release
	EndIf
	
Return

 
*
*ArticulosGrabar 
*Se ejecuta cada vez que se Crea un Articulo
Procedure ArticulosGrabar
LParameter oForma
		
	With oForma	
	
		Sele Arts
			Replace FechaNew	with	iif(Empty(Arts.FechaNew),Date(),Arts.FechaNew)
	
	Endwith
		
Return


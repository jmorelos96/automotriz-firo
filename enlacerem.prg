Function EnlaceRem
lParameters cNumdoc

	cNumdoc = cNumdoc
	
	Create Cursor Tempg(;
	DocAnt	C(15),;
	Lista	L(1))
	
	Index on DocAnt Tag DocAnt
	Set order to DocAnt
	
	csql =  '  Select * From Movim ' + ;  
 	     		 '  Where ' + ;
        	  '  Movim.Numdoc  = m.Val1 And ' + ;
        	  '  Not Empty(Movim.Numart) And ' + ;
        	  '  Movim.Tipodoc  = " R" ' + ;
        	   ' Order By Movim.Numpar '
                  
					if not sqlmsl( 'TempRemi', csql, cNumdoc  )
						return             
					Endif
	
	Sele TempRemi
	Scan
		
		Sele Tempg
			Append Blank
				Replace	DocAnt	with	TempRemi.Tipodoc + TempRemi.Numdoc + TempRemi.Numpar,;
						Lista	with	.F.
		
		Sele TempRemi
		
	Endscan
	
	Sele Temp 
	nRec = Reccount()
	Scan
		
		cDocAnt = Temp.DocAnt
		
		Sele Tempg
		=Seek(cDocAnt,'Tempg','DocAnt')
		iF found()
			
			Sele Tempg	
				Replace	Lista	with	.T.
		
		Endif
		
		Sele Temp
	
	Endscan
		
	Sele Temp
	GOTo nRec
	
	Sele Tempg
	Scan	
		iF Tempg.Lista = .F.
			cEnlace="No Pasa"
			EXIT
		Else 
			cEnlace="Pasa"
		Endif
	Endscan
		
	*Obtener area actual
	nArea = Select(0)
	
	* Regresarme al area original
	Select (nArea)
	
* Regresar el numero de hojas
Return (cEnlace)
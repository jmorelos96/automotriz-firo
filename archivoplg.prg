Procedure Archivoplg

	* Configurar menu de opciones
	IF NIVEL <> ' VEND' 
	    AddBar('Ventas','Grafica Comparativa anual',            'DoForm("grafica")')
	ENDIF

	IF NIVEL <> ' VEND'     
		AddBar('Cobranza','Comisiones sobre Cobranza y caja', 'DoForm("ComFiro")')
	ENDIF

	IF NIVEL <> ' VEND' 
	    AddBar('Ventas','Importador de articulos',            'DoForm("imparts")')
	ENDIF

	IF NIVEL = 'SUPER' 
	    AddBar('Ventas','Importador de Clientes',            'DoForm("impcli")')
	ENDIF

	IF NIVEL = ' VEND' 

	    RELEASE BAR 5 OF COBRANZA
	    RELEASE BAR 6 OF COBRANZA
	ENDIF

	IF NIVEL <> ' VEND' 
	    AddBar('Ventas','Consult\<a de Direccion',            'DoForm("ConsDire")')
	    AddBar('Ventas','Resultados \<x Sucursal',  'DoForm("EdoResxs")')
	    AddBar('cuentaspor','Consulta de Compras Pagadas',       'DoForm("ComPag")')
	    AddBar('cuentaspor','Consulta de Gastos \<Pagados', 'DoForm("GasPag")')
	ENDIF

	********************************************************************************************

	*ventas
	AddBar('Ventas','Catalogo de Articulos con Foto',  'DoForm("fotoscat")')
	AddBar('Ventas','Cargar Vendedores de Excel', 'DoForm("Vendxfac")')
	AddBar('Ventas','\-','')	
    AddBar('Ventas','Consulta de Articulos Nuevos', 'DoForm("ConsArtsNew")')
    AddBar('Ventas','\-','')	
    AddBar('Ventas','Consulta de Remisiones Facturadas', 'DoForm("CatRemFac")')

	*inventario
	AddBar('Inventario','\-','')  
	AddBar('Inventario', 'Agregar Fotos', 'DoForm("agregafoto")')
	AddBar('inventario','Consulta de existencia por sucursal', 'DoForm("existesuc")')
	AddBar('Inventario','Cargar datos adicionales de art�culos','Do Form areyvol','','','')

	*compras
	AddBar('Compras','\-','')  
	AddBar('Compras','Compras dinamicas', 'DoForm("compdina")')
	AddBar('Compras','\-','' )
	AddBar('Compras','Alta de Proveedores', 'DoForm("Catprov2",1)')
	AddBar('Compras','Consulta de Proveedores', 'DoForm("Catprov")')
	AddBar('Compras','Consulta de Compras Pagadas',        'DoForm("ComPag")')
	AddBar('Compras','Datos Adicionales proveedor', 'DoForm("Datprov")')
	AddBar('Compras','\-','' )
	AddBar('Compras','Precios por Proveedor', 'DoForm("prexProv")')
	AddBar('Compras','\-','' )
	AddBar('Compras','Consulta detallada de Compras', 'DoForm("ComxArt")')

	*gastos
	AddBar('Gastos','\-','')  
	AddBar('Gastos','Consulta de Gastos \<Pagados',           'Do Form GasPag')

	*contabilidad
	AddBar('contabilid','\-','')
	AddBar('contabilid','Estado de Resultados 12 meses', 'DoForm("edores12")')
	AddBar('contabilid','Balance General 12 meses', 'DoForm("balgral12")')
	AddBar('contabilid','Balanza de Comprobaci�n', 'DoForm("balanza")')
	AddBar('contabilid','Libro Diario de P�lizas', 'DoForm("diariopol")')
	AddBar('contabilid','\-','')
	AddBar('contabilid','Consult\<a de Direccion','DoForm("ConsDire")')	

	*COBRANZA
	AddBar('Cobranza','\-','' )
	AddBar('Cobranza','Estado de Cuenta Firo','DoForm("edoctam")')
	AddBar('Cobranza','\-','' )
	AddBar('Cobranza','P�liza de Ingresos del d�a','DoForm("Poling")' )
	AddBar('Cobranza','\-','' )
	AddBar('Cobranza','Gerente de Cobranza','DoForm("Saldocxc")' )

	*CUENTASPORPAGAR
	AddBar('Cuentaspor','\-','')
	AddBar('Cuentaspor','Estado de Cuenta Firo','DoForm("edoctacxp")')
	AddBar('cuentaspor','Consulta de Compras y Gastos \<Pagados', 'DoForm("Gascom")')
	AddBar('Cuentaspor','\-','') 
	AddBar('cuentaspor','Registro de anticipos', 'DoForm("regan")')
	AddBar('cuentaspor','Aplicaci�n de Anticipos y notas de cr�dito', 'DoForm("aplican")')


	* M O D U L O   D E   R U T A S   *
	* Agregar el menu principal
	Define Pad PadProd of _MSysMenu Prompt "Ru\<tas"  Key ALT-d
	On Pad PadProd of _MSysMenu Activate PopUp Rutas
			
	Define PopUp Rutas Margin Relative Shadow 
	AddBar('Rutas','Cat�logo de Vehiculos',' Do form catcam ')
	AddBar('Rutas','Cat�logo de Choferes','do form  catoper')
	AddBar('Rutas','Cat�logo de Paqueterias','do form  catpaq')
	AddBar('Rutas','Bitacora de Rutas','do form Rutas')

	*quita renglones
	*Release Bar 1 Of Compras
	Release Bar 6 Of Cobranza
		
endproc